from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Restaurant(models.Model):
    name = models.CharField(max_length = 100)
    cuisine = models.CharField(max_length = 50)
    class Meta:
        verbose_name = ("Restaurant")
        verbose_name_plural = ("Restaurants")

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length = 100)
    description = models.CharField(max_length = 200)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    restaurant = models.ForeignKey('Restaurant', on_delete=models.CASCADE)

    class Meta:
        verbose_name = ("Item")
        verbose_name_plural = ("Items")

    def __str__(self):
        return self.name


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField('Item')
    created_at = models.DateTimeField(auto_now=True)

    

    class Meta:
        verbose_name = ("Order")
        verbose_name_plural = ("Orders")

    def __str__(self):
        return self.name
