from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Permission
from menus.models import *
from menus.forms import *

# Create your views here.

# Signup
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            if user is not None:
                print("---Creds given---")
                user.has_perm('menus.add_order')
                user.has_perm('menus.view_order')
                user.save()
            else:
                print("---Creds denied---")
            return redirect('login/')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


# List Restaurants
def restaurantList(request):
    restaurants = Restaurant.objects.all()
    return render(request, 'index.html', context={'res':restaurants})


# Create New Restaurant
# @login_required
@staff_member_required
def createRestaurant(request):
    form = RestaurantForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('/')
    
    return render(request, 'createRestaurant.html', {'form': form})


# Updating a Restaurant
# @login_required
@staff_member_required
def updateRestaurant(request, id):
    restaurant = Restaurant.objects.get(id=id)
    form = RestaurantForm(request.POST or None, instance=restaurant)
    
    if form.is_valid():
        form.save()
        return redirect('/')
    
    return render(request, 'createRestaurant.html', {'form': form, 'restaurant': restaurant})


# Deleting a Restaurant
# @login_required
@staff_member_required
def deleteRestaurant(request, id):
    restaurant = Restaurant.objects.get(id=id)

    if request.method == 'POST':
        restaurant.delete()
        return redirect('/')
    
    return render(request, 'deleteRestaurant.html', {'restaurant': restaurant})


# Listing all Items
def listItems(request, id):
    res = Restaurant.objects.get(id=id)
    items = Item.objects.filter(restaurant=id).all()
    return render(request, 'listItems.html', context={'items': items, 'res': res})


# Adding an Item
# @login_required
@staff_member_required
def addItem(request, id):
    restaurant  = Restaurant.objects.get(id=id)
    form = ItemForm(request.POST or None, initial={'restaurant': id})

    if form.is_valid():
        form.save()
        return redirect('/')
    
    return render(request, 'addItem.html', {'form': form})


# Updating an Item
# @login_required
@staff_member_required
def updateItem(request, res_id, item_id):
    restaurant = Restaurant.objects.get(id=res_id)
    item = Item.objects.get(id=item_id)
    form = ItemForm(request.POST or None, instance=item)

    if form.is_valid():
        form.save()
        return redirect('/')
    
    return render(request, 'addItem.html', {'form': form, 'item': item, 'res': restaurant})


# Deleting an Item
# @login_required
@staff_member_required
def deleteItem(request, res_id, item_id):
    restaurant = Restaurant.objects.get(id=res_id)
    item = Item.objects.get(id=item_id)

    if request.method == 'POST':
        item.delete()
        return redirect('/')
    
    return render(request, 'deleteItem.html', {'item': item})


# Functions for order
# add_to_order
# remove_from_order
# proceed_checkout
# view_order

# TLDR
# Create an order
# View order by the user